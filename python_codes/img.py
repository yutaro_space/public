### showImg
# ndarray: 2d image array
def showImg(ndarray,color='jet',title='test', vmin=0, vmax=0,saveName='false'):
	# import file
	import matplotlib.pyplot as plt
	import mpl_toolkits.axes_grid1
	fig = plt.figure()
	fig.set_size_inches(5,5)  
	ax = fig.add_subplot(1,1,1)
	ax.set_title(title.format(fontsize=12))
	divider = mpl_toolkits.axes_grid1.make_axes_locatable(ax)
	cax = divider.append_axes('right', '5%', pad='3%')
	im = ax.imshow(ndarray, cmap=color)
	fig.colorbar(im, cax = cax)
	if vmin != vmax:
		im.set_clim(vmin,vmax)
	fig.tight_layout()
	if saveName == 'false':
		plt.show()
	else:
		plt.savefig(saveName, bbox_inches="tight", pad_inches=0.0, dpi=1000)

### showImgs
# imgs(n,i,j)
def showImgs(imgs, color='gray', column=1, vmin=0, vmax=0, savepath='false'):
	import matplotlib.pyplot as plt
	import mpl_toolkits.axes_grid1
	import numpy as np
	# preparation
	num = imgs.shape[0]
	row = num // column + 1
	# plotting
	fig = plt.figure()
	fig.set_size_inches(5*column,5*row)  
	for i in range(num):  
		ax = 'ax' + str(i)
		ax = fig.add_subplot(row,column,i+1)
		ax.set_title('{}'.format(i, fontsize=12))
		divider = mpl_toolkits.axes_grid1.make_axes_locatable(ax)
		cax = divider.append_axes('right', '5%', pad='3%')
		im = ax.imshow(imgs[i,:,:], cmap=color)
		fig.colorbar(im, cax = cax)
		if vmin != vmax:
			im.set_clim(vmin,vmax)
	fig.tight_layout()
	if savepath != 'false':
		plt.savefig(savepath, dpi=300, bbox_inches='tight')
	else:
		plt.show

### saveImgs_gif
# dirPath = 'DIRECTORPY-PATH/'
# extension = 'png'
# saveName = 'GIFNAME.gif'
def saveImgs_gif(dirPath, extension, saveName, duration=500):
	from PIL import Image
	from function import data
	imageNames = data.getFilenames(dirPath + '*.' + extension)
	frames = []
	for imageName in imageNames:
		new_frame = Image.open(imageName)
		frames.append(new_frame)
	# save as gif
	# duration[ms]
	print(dirPath+saveName)
	frames[0].save(dirPath+saveName,format='GIF',append_images=frames[1:],save_all=True,duration=duration,loop=0)

### plot lineGraphs
# lines: tupple of several lines
# legends: tuple(list of names, loc)
def lineGraphs(x,lines,bbox_to_anchor=(1.05, 1),loc='upper left',labels=False,colors=False):
	import matplotlib.pyplot as plt
	import random
	if colors == False:
		colors = ()
		for i in range(len(lines)):
			colors += ("#"+''.join([random.choice('0123456789ABCDEF') for j in range(6)]),)
	for i in range(len(lines)):
		line = lines[i]
		if labels==False:
			plt.plot(x, line, color=colors[i])
		elif labels!=False:
			plt.plot(x, line, color=colors[i], label=labels[i])
			plt.legend(bbox_to_anchor=bbox_to_anchor,loc=loc)
		else:
			print('error: undefined situation of labels and colors')
	plt.show()