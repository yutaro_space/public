### makedir
# make a directory 
def makedir(directory):
	import os
	if not os.path.exists(directory):
		os.makedirs(directory)


### get all file names in a directory
# inputDir = './src/*'
def getFilenames(inputDir):
	import glob
	allFilePaths = glob.glob(inputDir)
	allFilePaths = sorted(allFilePaths)
	return(allFilePaths)